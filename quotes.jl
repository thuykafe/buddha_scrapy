{"quote": "\u201cThere are only two ways to live your life. One is as though nothing is a miracle. The other is as though everything is a miracle.\u201d", "author": "Albert Einstein"}
{"quote": "\u201cThere are only two ways to live your life. One is as though nothing is a miracle. The other is as though everything is a miracle.\u201d", "author": "Albert Einstein"}
{"quote": "\u201cThe fear of death follows from the fear of life. A man who lives fully is prepared to die at any time.\u201d", "author": "Mark Twain"}
