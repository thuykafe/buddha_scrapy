# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html

import json
from scrapy.exceptions import DropItem
from scrapy.exporters import BaseItemExporter


class BuddhaScrapyPipeline(object):
    def open_spider(self, spider):
        self.file = open('canons.json', 'w')

    def close_spider(self, spider):
        self.file.close()

    def process_item(self, item, spider):
        line = json.dumps(dict(item)) + "\n"
        self.file.write(line)
        if item['link']:
            return item['link']
        else:
            raise DropItem("Missing link in %s" % item)


class QuoteExportPipeline(object):

    def open_spider(self, spider):
        self.file = open('quotes.json', 'w')

    def close_spider(self, spider):
        self.file.close()

    def process_item(self, item, spider):
        line = json.dumps(dict(item))+'\n'
        self.file.write(line)
        return item['quote']


# def open_spider(self, spider):
#         self.year_to_exporter = {}

#     def close_spider(self, spider):
#         for exporter in self.year_to_exporter.values():
#             exporter.finish_exporting()
#             exporter.file.close()

#     def _exporter_for_item(self, item):
#         year = item['year']
#         if year not in self.year_to_exporter:
#             f = open('{}.xml'.format(year), 'wb')
#             exporter = XmlItemExporter(f)
#             exporter.start_exporting()
#             self.year_to_exporter[year] = exporter
#         return self.year_to_exporter[year]

#     def process_item(self, item, spider):
#         exporter = self._exporter_for_item(item)
#         exporter.export_item(item)
#         return item
