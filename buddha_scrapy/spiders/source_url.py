# -*- coding: utf-8 -*-
import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule

from ..items import Quote


class SourceUrlSpider(CrawlSpider):
    name = 'source_url'
    # allowed_domains = ['kinh.kinhphat.org']
    start_urls = [
        # 'http://kinh.kinhphat.org/nhac/',
        'http://quotes.toscrape.com/'
    ]

    rules = (
        Rule(LinkExtractor(allow=r'life/'), callback='parse_item', follow=True),
    )

    def parse_item(self, response):
        i = Quote()
        i['quote'] = response.xpath(
            '//div[@class="quote"]/span/text()').extract_first()
        i['author'] = response.xpath(
            '//small[@class="author"]/text()').extract_first()
        # print i
        #i['domain_id'] = response.xpath('//input[@id="sid"]/@value').extract()
        #i['name'] = response.xpath('//div[@id="name"]').extract()
        #i['description'] = response.xpath('//div[@id="description"]').extract()
        yield i
