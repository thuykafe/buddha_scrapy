# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class BuddhaScrapyItem(scrapy.Item):
    # define the fields for your item here like:
    # source_path = scrapy.Field()
    link = scrapy.Field()
    # category = scrapy.Field()


class Quote(scrapy.Item):
    quote = scrapy.Field()
    author = scrapy.Field()
    tags = scrapy.Field()
